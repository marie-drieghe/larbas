<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'middleware' => 'under-construction',
], function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');

    Route::group([
        'prefix' => 'dashboard',
        'middleware' => ['auth'],
    ], function () {
        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    });

    require __DIR__.'/auth.php';
});
