#!/bin/sh

#
# Post deploy script for a Laravel installation.
# This script should be invoked from the root folder of your installation
#
# Usage: post-deploy.sh
# -e=<environment>   define the environment you are deploying
# -c                 skip clearing the cache
# -o                 skip orm define command
# -a                 skip assets deploy
# -w                 skip warm cache
#
# For example a post deploy for the staging environment
# ./post-deploy.sh -e=staging
#
# When exists, application/htaccess-staging will be copied to public/.htaccess
# The same for application/robots-staging which will be copied to public/robots.txt
#

# Initialize parameters
ENVIRONMENT="local"
PHP="php"
SKIP_CLEAR_CACHE=0
SKIP_MIGRATION=0
SKIP_ASSETS_DEPLOY=0
SKIP_WARM_CACHE=0
SKIP_LANGIMPORT=0

# Gather parameters from arguments
for i in "$@"
do
    case $i in
        -e=*|--environment=*)
            ENVIRONMENT="${i#*=}"
            shift
        ;;
        -p=*|--php-version=*)
            PHP_VERSION="${i#*=}"
            shift
        ;;
        -c|--skip-clear-cache)
            SKIP_CLEAR_CACHE=1
            shift
        ;;
        -m|--skip-migration)
            SKIP_MIGRATION=1
            shift
        ;;
        -a|--skip-assets)
            SKIP_ASSETS_DEPLOY=1
            shift
        ;;
        -w|--skip-warm-cache)
            SKIP_WARM_CACHE=1
            shift
        ;;
        -l|--skip-language-import)
            SKIP_LANGIMPORT=1
            shift
        ;;
        *)
            # unknown option
        ;;
    esac
done

#
# Executes a command
# $1 Command to execute
# $2 Information message before the command
#
executeCommand() {
    echo $2
    $1
    if [ $? -ne 0 ]; then
        exit $?
    fi
}

# Put Laravel in maintenance mode
executeCommand "$PHP artisan down" "Put the site in maintenance mode"

# Run migrations first in case there are migrations that are required by the libraries that will be installed
if [ $SKIP_MIGRATION -eq 0 ]; then
    executeCommand "$PHP artisan migrate --force" "Running migrations..."
fi

# Run composer:
executeCommand "composer install --optimize-autoloader --no-dev" "Installing composer requirements and optimizing autoloader..."
#executeCommand "composer install --no-dev" "Installing composer requirements..."
#executeCommand "composer dump-autoload --optimize" "Optimizing autoloader"

# .env file
FILE=".env.$ENVIRONMENT"
if [ -f $FILE ]; then
    executeCommand "cp $FILE .env" "Copying .env file..."
fi

if [ $SKIP_CLEAR_CACHE -eq 0 ]; then
    executeCommand "$PHP artisan cache:clear" "Clearing cache..."
fi

# Run migrations again in case composer has installed new migrations.
if [ $SKIP_MIGRATION -eq 0 ]; then
    executeCommand "$PHP artisan migrate --force" "Running migrations..."
fi
if [ $SKIP_WARM_CACHE -eq 0 ]; then
    #executeCommand "$PHP artisan route:cache" "Warming route cache..."
    executeCommand "$PHP artisan config:cache" "Warming config cache..."
fi
#if [ $SKIP_ASSETS_DEPLOY -eq 0 ]; then
#
#fi

#FILE="application/htaccess-$ENVIRONMENT"
#if [ -f $FILE ]; then
#    executeCommand "cp $FILE public/.htaccess" "Copying .htaccess file..."
#fi

#FILE="application/worker-$ENVIRONMENT.sh"
#if [ -f $FILE ]; then
#    executeCommand "cp $FILE application/worker.sh" "Copying worker.sh file..."
#fi

#FILE="application/robots-$ENVIRONMENT"
#if [ -f $FILE ]; then
#    executeCommand "cp $FILE public/robots.txt" "Copying robots.txt file..."
#fi

if [ $SKIP_LANGIMPORT -eq 0 ]; then
    executeCommand "$PHP artisan statik:importlang" "Importing language files into db..."
fi

# Bring Laravel back up from maintenance mode
executeCommand "$PHP artisan up" "Bring the site back up"

$PHP artisan inspire

echo "Done"
