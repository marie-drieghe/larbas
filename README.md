# Laravel base install

This is a starting point to set up a new laravel project.

The repository contains several branches with different functionalities added to the base install. The bare bones base
install is available on the master branch.

## Installation for your new project

- Clone the repository
- Remove the .git directory and this README.md (write a project specific one! ;-)

Setup laravel project:
- Copy `.env.example` to `.env` and edit to your needs
- Run `php artisan key:generate` to generate a unique application ID (to avoid all our projects have the same key)
- Run `composer install`

Locking the repo on staging
- Run `php artisan code:set 123456` to set the code of the staging lock. This is set to __574716__ (STATIK in L33T speech) by default.


## Installed libraries

### coconutcraig/laravel-postmark
Provides the application the option to send mails using postmark. By default, the laravel sandbox key is set.


