<?php

namespace App\Http\Middleware;

use Closure;
use LarsJanssen\UnderConstruction\UnderConstruction as VendorUnderConstruction;

class UnderConstruction extends VendorUnderConstruction
{
    public function handle($request, Closure $next)
    {
        $allowedIPs = [
            '81.82.199.174',
            '94.225.165',
            '79.174.134.38',
            '79.174.134.39',
            '127.0.0.1',
            '::1',
        ];

        if (in_array(request()->ip(), $allowedIPs)) {
            return $next($request);
        }

        return parent::handle($request, $next);
    }
}
